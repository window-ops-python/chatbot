# Chatbot

This is a basic chatbot in PySimpleGUI using microsoft/DialoGPT-large.

Its functions are severely limited and prone to bugs.

## Installation

To run this project, you will need to have Python, PySimpleGUI, transformers and torch installed on your computer. You can download Python from the official website, and install the dependencies using pip:

```
pip install PySimpleGUI, transformers, torch
```

## Usage

To run the project, simply run the `app.py` file using Python:

```
python app.py
```
